export class MerchantTransactionResponse {
    public merchantKey: string;
    public merchantName: string;
    public description: string;
    public totalPrice: number; 
}