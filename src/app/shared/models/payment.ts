export class Payment  {
    public key: string;
    public expiryDate: string;
    public pvn: string;
    public reference: string;
    public nonce: string;
}
