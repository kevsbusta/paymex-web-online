export class PaymentPost {    
    public key: string;
    public expiry_date: string;
    public pvn: string;
    public reference: string;
}
