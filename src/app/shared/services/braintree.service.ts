import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { CheckoutResponsePost } from '../models/checkout-response-post';
import * as braintree from 'braintree-web';
//import * as dropinInstance from 'braintree-web-drop-in';

@Injectable()
export class BraintreeService {
  private url = 'http://58e9a765.ngrok.io/checkouts/new';

  constructor(private http: HttpClient) { 
  }

  public getClientToken(): Observable<CheckoutResponsePost> {
    return this.http.get<CheckoutResponsePost>(this.url);
  }

  public getNonce(clientToken: string): Observable<string> {
    let nonce: string = '';
    console.log('clienttoken', clientToken)
    
    braintree.client.create({
      authorization: clientToken
    }, (err, dropinInstance) => {
      if (err) {
        console.error("error",err);
        return;
      }
      
      dropinInstance.requestPaymentMethod((err: any, payload: any) => {
        if (err) {
          // Handle errors in requesting payment method
        }

        nonce = payload.nonce;
        console.log('payload', payload);
      });
    }, (err, response) => { console.log(err)});

    return of(nonce);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
