import { TestBed, inject } from '@angular/core/testing';

import { MerchantTransactionService } from './merchant-transaction.service';

describe('MerchantTransactionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MerchantTransactionService]
    });
  });

  it('should be created', inject([MerchantTransactionService], (service: MerchantTransactionService) => {
    expect(service).toBeTruthy();
  }));
});
