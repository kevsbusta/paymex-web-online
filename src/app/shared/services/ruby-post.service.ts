import { Injectable } from '@angular/core';
import { Payment } from '../models/payment';
import { PaymentPost } from '../models/payment-post';
import { CheckoutResponse } from '../models/checkout-response';

@Injectable()
export class RubyPostService {

  constructor() { }

  public rubifyPayment(payment: Payment): PaymentPost {
    let result = new PaymentPost();
    result.expiry_date = payment.expiryDate;
    result.key = payment.key.replace(/-/g, "");
    result.reference = "MR-04fae173-22";

    return result;
  }
}
