import { TestBed, inject } from '@angular/core/testing';

import { RubyPostService } from './ruby-post.service';

describe('RubyPostService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RubyPostService]
    });
  });

  it('should be created', inject([RubyPostService], (service: RubyPostService) => {
    expect(service).toBeTruthy();
  }));
});
