import { InMemoryDbService } from 'angular-in-memory-web-api';

import { MerchantTransactionResponse } from '../models/merchant-transaction-response';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const merchantTransactionResponse: MerchantTransactionResponse = {
      merchantName: "Pazada",
      merchantKey: "aaaaa",
      description: "Sakay na!",
      totalPrice: 0
    };

    return { merchantTransactionResponse };
  }
}