import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MerchantTransactionResponse } from '../../shared/models/merchant-transaction-response';
import { MerchantTransactionService } from '../../shared/services/merchant-transaction.service';
import { PaymentService } from '../../shared/services/payment.service';
import { Payment } from '../../shared/models/payment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { BraintreeService } from '../../shared/services/braintree.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-send-payment',
  templateUrl: './send-payment.component.html',
  styleUrls: ['./send-payment.component.scss']
})
export class SendPaymentComponent implements OnInit {
  merchantTransactionResponse: MerchantTransactionResponse;
  payment: Payment;
  clientToken: string;
  private mask: any = [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(
    private merchantTransactionService: MerchantTransactionService,
    private paymentService: PaymentService,
    private braintreeService: BraintreeService,
    private toastr: ToastsManager,
    vcr: ViewContainerRef) { 
      this.toastr.setRootViewContainerRef(vcr);
    }

  ngOnInit() {
    this.braintreeService.getClientToken().subscribe(checkout => this.clientToken = checkout.client_token);
    this.getMerchantTransaction();
    this.payment = new Payment();
  }

  getMerchantTransaction(): void {
    this.merchantTransactionService.getMerchantTransaction()
      .subscribe(merchantTransactionRespones => this.merchantTransactionResponse = merchantTransactionRespones);
  }

  submitPayment(): void {
    // this.braintreeService.getNonce(this.clientToken).subscribe((nonce) => {
    //   this.payment.nonce = nonce;
    //   console.log(nonce);
      this.paymentService.submit(this.payment).subscribe((paymentResponse) => {
        this.toastr.success("Code validated", "Success!");
      });
    // });
  }
}
