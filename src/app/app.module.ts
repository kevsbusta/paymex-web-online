import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TextMaskModule } from 'angular2-text-mask';
import { Client } from 'braintree-web';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { SendPaymentComponent } from './pages/send-payment/send-payment.component';
import { MerchantTransactionService } from './shared/services/merchant-transaction.service';
import { PaymentService } from './shared/services/payment.service';
import { RubyPostService } from './shared/services/ruby-post.service';
import { BraintreeService } from './shared/services/braintree.service';


@NgModule({
  declarations: [
    AppComponent,
    SendPaymentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TextMaskModule,
    ToastModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [
    MerchantTransactionService, 
    PaymentService, 
    RubyPostService,
    BraintreeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
